DM
====
Slim/Slimlock
XDM
GDM/KDM/LightDM

Others
====
Plymouth

Resolutions
====
Original wallpaper resolution: 2560x1440
1280x1024 or 2560x2048 5:4 SXGA
1600x1200 or 2048x1536 4:3 standard
1920x1200 or 2560x1600 16:10 widescreen
1920x1080 16:9 FullHD