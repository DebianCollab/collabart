#!/bin/sh

rm -R pngs
mkdir pngs
find . -name "*.svg" -exec sh -c 'inkscape {} -C -e pngs/$(basename {} .svg).png' \;
